using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data;

namespace Homework3
{
    public partial class Form1 : Form
    {
        MySqlConnection conn; //连接数据库对象
        MySqlDataAdapter adapter; //适配器变量
        DataSet set;  //临时数据集
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sqlStr = "select * from stuinfo";
            //创建适配器对象，其是连接数据集以及数据库之间的一个接口
            adapter = new MySqlDataAdapter(sqlStr, conn);
            //数据集、本地微型数据库可以存储多张表。
            set = new DataSet();
            //从数据库的stuinfo表中取出数据
            adapter.Fill(set, "stuinfo");
            //将取出的数据做为dataGridView1的数据源
            dataGridView1.DataSource = set;
            dataGridView1.DataMember = "stuinfo";
        }

        private void button2_Click(object sender, EventArgs e)
        {

            if (adapter == null || set == null) //先导入数据才能更新数据。
            {
                MessageBox.Show("请先导入数据");
                return;
            }
            try
            {
                string msg = "确实要更新吗？";
                if (1 == (int)MessageBox.Show(msg, "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation))
                {
                    MySqlCommandBuilder builder = new MySqlCommandBuilder(adapter); //命令生成器。                 
                    adapter.Update(set, "stuinfo");
                    MessageBox.Show("更新成功", "提示");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "错误信息");
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            string sql = textBox1.Text;
            adapter = new MySqlDataAdapter(sql, conn);
            //数据集、本地微型数据库可以存储多张表。
            set = new DataSet();
            adapter.Fill(set, "stuinfo");
            dataGridView1.DataSource = set;
            dataGridView1.DataMember = "stuinfo";
        }

        private void button4_Click(object sender, EventArgs e)
        {

            string connStr = "server = localhost; user = root; database = students; port = 3306; password = 5250";
            //创建连接数据库的对象
            conn = new MySqlConnection(connStr);
            try
            {
                conn.Open();
                MessageBox.Show("连接成功");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int i = dataGridView1.SelectedCells[0].RowIndex;
            dataGridView1.Rows.RemoveAt(i);
        }
    }
}